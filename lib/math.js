class LinkedListNode {
    constructor(_val) {
        this.previous = undefined;
        this.next = undefined;
        this.val = _val;
    }
}
export class LinkedList {
    constructor() {
        this.head = undefined;
        this.tail = undefined;
    }
    push(val) {
        if(this.head == undefined) {
            this.head = new LinkedListNode(val);
            this.tail = this.head;
        } else {
            let prevHead = this.head;
            this.head = new LinkedListNode(val);
            this.head.next = prevHead;
            this.head.next.prev = this.head;
        }
    }
    pop() {
        if(this.head != undefined) {
            let val = this.head;
            if(this.head == this.tail) {
                this.head = undefined;
                this.tail = undefined;
            } else {
                this.head = this.head.next;
                this.head.prev = undefined;
            }
            return val;
        }
        return undefined;
    }
    append(val) {
        if(this.tail == undefined) {
            this.head = new LinkedListNode(val);
            this.tail = this.head;
        } else {
            let prevTail = this.tail;
            this.tail = new LinkedListNode(val);
            this.tail.prev = prevTail;
            this.tail.prev.next = this.tail;
        }
    }
    merge(otherList) {
        if(this.head == this.tail) {
            this.head = otherList.head;
        }
        this.tail.next = otherList.head;
        otherList.head.prev = this.tail;
        this.tail = otherList.tail;
    }
    count() {
        let sum = 0;
        let node = this.head;
        while(node != undefined) {
            sum++;
            node = node.next;
        }
        return sum;
    }
    forEach(cb) {
        let node = this.head;
        while(node != undefined) {
            cb(node.val);
            node = node.next;
        }
    }
    find(eq) {
        let node = this.head;
        while(node != undefined) {
            if(eq(node.val)) {
                return node.val;
            }
            node = node.next;
        }
    }
}

export class SparseMatrix {
    constructor(x=0, y=0) {
        this.data = new LinkedList();
        this.defaultValue = undefined;
    }
    initialize(value) {
        this.defaultValue = value;
    }

    setValue(x, y, val) {
        let yList = this.data.find(el => el.x == x);
        if(yList == undefined) {
            yList = new LinkedList();
            this.data.append({x: x, list: yList });
        }
        let yVal = yList.find(el => el.y == y);
        if(yVal == undefined) {
            yList.append({y: y, val: val });
        }
    }

    getValue(x, y) {
        let yList = this.data.find(el => el.x == x);
        if(yList == undefined) {
            return this.defaultValue;
        }
        let yVal = yList.list.find(el => el.y == y);
        if(yVal == undefined) {
            return this.defaultValue;
        }
        return yVal.val;
    }
}
