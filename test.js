import path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs';
import TestClass from './testclass.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const args = process.argv.slice(2);

let flags = {
    year: undefined,
    test: undefined,
    verbose: false
}

parseArgs();
let testYears = []
if(flags.year == undefined) {
    let files = fs.readdirSync(__dirname, {withFileTypes: true});
    files = files.filter(dir => dir.isDirectory() && dir.name[0] != "." && !isNaN(dir.name));
    testYears = testYears.concat(files.map(dir => dir.name));
} else {
    testYears.push(flags.year);
}

// console.log(testYears);
for(let i=0; i<testYears.length; ++i) {
    await testYear(testYears[i]);
}

async function testYear(yearNumber) {
    let yearPath = path.join(__dirname, yearNumber);
    if(!fs.existsSync(yearPath) || !fs.lstatSync(yearPath).isDirectory()) {
        console.log(`Test year ${yearNumber} does not exist on the filesystem!`);
        return;
    }

    let tests = []
    if (flags.test == undefined) {
        let files = fs.readdirSync(yearPath);
        files = files.filter(file => path.extname(file));
        tests = tests.concat(files);
    } else {
        tests.push(flags.test);
    }
    // console.log(tests);
    for (let i=0; i<tests.length; ++i) {
        await runTest(yearNumber, tests[i]);
    }
}

async function runTest(yearNumber, filename) {
    console.log(`Running test ${yearNumber}/${filename}`);
    let testPath = path.join(__dirname, yearNumber, filename);
    if(!fs.existsSync(testPath) || !fs.lstatSync(testPath).isDirectory()) {
        console.log(`Test ${yearNumber}/${filename} does not exist on the filesystem!`);
        return;
    }
    let filePath = path.join(testPath, "index.js");
    if(!fs.existsSync(filePath) || !fs.lstatSync(filePath).isFile()) {
        console.log(`Test ${yearNumber}/${filename}/index.js does not exist on the filesystem!`);
        return;
    }

    console.log("Loading file:", path.relative(__dirname, filePath));
    process.chdir(`./${yearNumber}/${filename}`)
    const mod = await import("./" + path.relative(__dirname, filePath));
    // console.log(mod.default instanceof TestClass);
    // console.log(mod.default);
    if(mod.default instanceof TestClass) {
        mod.default.init();
        console.log("Test A: " + mod.default.testA());
        mod.default.init();
        console.log("Test B: " + mod.default.testB());
    }
    process.chdir(`../../`)
}

function parseArgs() {
    let skipNextArg = false;
    for(let i=0; i<args.length; ++i) {
        if(skipNextArg) {
            skipNextArg = false;
            continue;
        }
        switch(args[i]) {
            case "-y":
            case "--year":
                if(i < args.length-1) {
                    if(args[i+1][0] == "-") {
                        console.log(`Missing argument for ${args[i]}`);
                        process.exit(1);
                    }
                    flags.year = args[i+1]
                    skipNextArg = true;
                } else {
                    console.log(`Missing argument for ${args[i]}!`);
                    process.exit(1);
                }
                break;
            case "-t":
            case "--test":
                if(i < args.length-1) {
                    flags.test = args[i+1]
                    skipNextArg = true;
                } else {
                    console.log(`Missing argument for ${args[i]}!`);
                    process.exit(1);
                }
                break;
            case "-v":
            case "--verbose":
                flags.verbose = true;
                break;
            default:
                console.log(`Unknown cmd line flag: ${args[i]}. Skipping...`);
        }
    }
}
