import fs from 'fs';
import TestClass from "../testclass.js"
import { LinkedList, SparseMatrix } from "../lib/math.js"

class Day5TestClass extends TestClass {
    constructor() {
        super();
    }
    init() {
        const rawInput = fs.readFileSync('7_input.txt', 'utf-8');
        this.input = rawInput.split("\r\n")[0].split(',').map(num => parseInt(num));
    }
    testA() {
        this.init();

        let max = this.input.reduce((a,b) => Math.max(a, b));
        let min = this.input.reduce((a,b) => Math.min(a, b));

        let minSum = Number.MAX_VALUE;
        let alignPoint = -1;
        for(let i=min; i<=max; ++i) {
            let sum = 0;
            for(let crab of this.input) {
                let costToMove = Math.abs(crab - i);
                sum += costToMove;
            }
            if(sum<minSum) {
                minSum = sum;
                alignPoint = i;
            }
        };

        return minSum;
    }
    testB() {
        this.init();

        let max = this.input.reduce((a,b) => Math.max(a, b));
        let min = this.input.reduce((a,b) => Math.min(a, b));

        let minSum = Number.MAX_VALUE;
        let alignPoint = -1;
        for(let i=min; i<=max; ++i) {
            let sum = 0;
            for(let crab of this.input) {
                let distance = Math.abs(crab - i);
                let costToMove = (distance * (distance+1))/2;
                sum += costToMove;
            }
            if(sum<minSum) {
                minSum = sum;
                alignPoint = i;
            }
        };

        return minSum;
    }
}

export default new Day5TestClass();