import fs from 'fs';

const data = fs.readFileSync('3_input.txt', 'utf-8');
let lines = data.split("\n");

lines = lines.map(line => line.replace('\r', ''))

let startTime = Date.now();

function getBitCounts(arr) {
    let counts = [];
    for(let i=0; i<arr.length; ++i) {
        let bin = arr[i];
        for(let j=0; j<bin.length; ++j) {
            let bit = parseInt(bin[j])
            if(counts.length <= j) {
                counts[j] = [0, 0];
            }
            counts[j][bit] = counts[j][bit] + 1;
        }
    }
    return counts;
}

function bitsToDecimal(bits) {
    let num = 0;
    for(let i=0; i<bits.length; ++i) {
        if(bits[i] == 1) {
            num += Math.pow(2, bits.length - 1 - i);
        }
    }
    return num;
}

let counts = []
let matching = lines;
for(let i=0; i<counts.length; ++i) {
    counts = getBitCounts(matching);
    let mostCommonBit = counts[i][0] > counts[i][1] ? 0 : 1;
    matching = matching.filter(line => {
        return parseInt(line[i]) == mostCommonBit;
    });
    if(matching.length == 1) {
        break;
    }
}

let o2 = bitsToDecimal(matching[0])
console.log(o2, bitsToDecimal(matching[0]));

matching = lines;
for(let i=0; i<counts.length; ++i) {
    counts = getBitCounts(matching);
    let leastCommonBit = counts[i][0] > counts[i][1] ? 1 : 0;
    matching = matching.filter(line => {
        return parseInt(line[i]) == leastCommonBit;
    });
    if(matching.length == 1) {
        break;
    }
}
let co2 = bitsToDecimal(matching[0])
console.log(co2, bitsToDecimal(matching[0]));

console.log(o2 * co2);

console.log((Date.now() - startTime)/1000.);