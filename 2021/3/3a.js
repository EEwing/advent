import fs from 'fs';
import TestClass from "../testclass.js"

class Day3TestClass extends TestClass {
    testA() {
        const data = fs.readFileSync('3_input.txt', 'utf-8');
        let lines = data.split("\n");

        let counts = [];

        for(let i=0; i<lines.length; ++i) {
            let bin = lines[i].slice(0, -1);
            for(let j=0; j<bin.length; ++j) {
                let bit = parseInt(bin[j])
                if(counts.length <= j) {
                    counts[j] = [0, 0];
                }
                counts[j][bit] = counts[j][bit] + 1;
            }
        }

        let gamma = 0;
        let epsilon = 0;

        for(let i=0; i<counts.length; ++i) {
            let pos = counts[i]
            console.log(pos);
            if(pos[1] > pos[0]) {
                gamma += Math.pow(2, counts.length - i - 1);
            } else {
                epsilon += Math.pow(2, counts.length - i - 1);
            }
        }

        console.log(gamma, epsilon, gamma*epsilon);
    }

    testB() {

    }
}

export default new Day3TestClass();