import fs from 'fs';
import TestClass from "../testclass.js"

class Board {
    constructor() {
        this.data = [[]];
    }
    markNum(number) {
        for (let row of this.data) {
            for (let val of row) {
                if(val.value == number) {
                    val.marked = true;
                }
            }
        }
    }
    checkWin() {
        let win = false;
        for(let i=0; i<this.data.length; ++i) {
            win |= this.checkRow(i);
        }
        for(let i=0; i<this.data[0].length; ++i) {
            win |= this.checkCol(i);
        }
        return win;
    }
    checkRow(i) {
        let row = this.data[i]
        let win = true;
        for(let val of row) {
            win &= val.marked;
        }
        if(win) {
            console.log("Row", i, "wins!");
        }
        return win;
    }
    getCol(i) {
        let column = []
        for(let row of this.data) {
            column.push(row[i]);
        }
        return column;
    }
    checkCol(i) {
        let col = this.getCol(i);
        let win = true;
        for(let val of col) {
            win &= val.marked;
        }
        if(win) {
            console.log("Column", i, "wins!");
        }
        return win;
    }
    sumUnmarked() {
        let sum = 0;
        for (let row of this.data) {
            for (let val of row) {
                if(val.marked == false) {
                    sum += parseInt(val.value);
                }
            }
        }
        return sum;
    }
}

class Day3TestClass extends TestClass {
    testA() {
        const data = fs.readFileSync('4_input.txt', 'utf-8');
        let lines = data.split("\r\n");

        let numbers = lines[0].split(",");

        let boards = [];
        let currentBoard = new Board();
        let currentRow = 0;
        for (let i=2; i<lines.length; ++i) {
            if(lines[i] == "") {
                boards.push(currentBoard);
                currentBoard = new Board();
                currentRow = 0;
                continue;
            }
            currentBoard.data[currentRow] = [];
            let row = lines[i].split(/\s+/).filter(item => item != "");
            for(let j=0; j<row.length; ++j) {
                if(row[j] != "") {
                    currentBoard.data[currentRow][j] = { value: row[j], marked: false }
                }
            }
            currentRow++;
        }
        boards.push(currentBoard);

        let winningBoard = undefined;
        let lastNumber = undefined
        for(let i=0; i<numbers.length; ++i) {
            lastNumber = numbers[i]
            let foundWinner = false;
            console.log("Testing number:", lastNumber);
            for(let j=0; j<boards.length; ++j) {
                let board = boards[j];
                board.markNum(lastNumber);
                let win = board.checkWin();
                console.log("board", j, "win:", win);
                if (win) {
                    console.log("win!", board.data);
                    winningBoard = board;
                    foundWinner = true;
                    break;
                }
            }
            if(foundWinner) break;
        }

        if(winningBoard == undefined) {
            return -1;
        }

        console.log(winningBoard.sumUnmarked(), lastNumber);
        return winningBoard.sumUnmarked() * lastNumber;
    }

    testB() {
     const data = fs.readFileSync('4_input.txt', 'utf-8');
        let lines = data.split("\r\n");

        let numbers = lines[0].split(",");

        let boards = [];
        let currentBoard = new Board();
        let currentRow = 0;
        for (let i=2; i<lines.length; ++i) {
            if(lines[i] == "") {
                boards.push(currentBoard);
                currentBoard = new Board();
                currentRow = 0;
                continue;
            }
            currentBoard.data[currentRow] = [];
            let row = lines[i].split(/\s+/).filter(item => item != "");
            for(let j=0; j<row.length; ++j) {
                if(row[j] != "") {
                    currentBoard.data[currentRow][j] = { value: row[j], marked: false }
                }
            }
            currentRow++;
        }
        boards.push(currentBoard);
        console.log(boards.length);

        let winningBoard = undefined;
        let lastNumber = undefined
        for(let i=0; i<numbers.length; ++i) {
            lastNumber = numbers[i]
            let foundWinner = false;
            console.log("Testing number:", lastNumber);
            for(let j=boards.length-1; j>=0; --j) {
                let board = boards[j];
                board.markNum(lastNumber);
                let win = board.checkWin();
                if (win) {
                    console.log(`bingo on board ${j}!`);
                    if(boards.length != 1) {
                        boards.splice(j, 1);
                    } else {
                        winningBoard = board;
                        break;
                    }
                    continue;
                }
            }
            if(winningBoard != undefined) {
                break;
            }
        }

        console.log(winningBoard, lastNumber);

        if(winningBoard == undefined) {
            return -1;
        }

        return winningBoard.sumUnmarked() * lastNumber;
    }
}

export default new Day3TestClass();