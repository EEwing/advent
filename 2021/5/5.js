import fs from 'fs';
import TestClass from "../testclass.js"
import { LinkedList, SparseMatrix } from "../lib/math.js"

const from = 0;
const to = 1;

class Day5TestClass extends TestClass {
    testA() {
        const rawInput = fs.readFileSync('5_input.txt', 'utf-8');
        let input = rawInput.split("\r\n");

        let data = [[]];
        const dataSize = 1000;
        for(let i=0; i<dataSize; ++i) {
            data[i] = []
            for(let j=0; j<dataSize; ++j) {
                data[i][j] = 0;
            }
        }

        let sum = 0;
        for(let line of input) {
            let coords = line.split(" -> ");
            coords = coords.map(el => el.split(",").map(val => parseInt(val)));
            // console.log(coords);
            let horiz = coords[from][1] == coords[to][1];
            let vert = coords[from][0] == coords[to][0];
            // console.log(horiz && !vert ? "horiz" : vert && !horiz ? "vert" : "both");
            if(horiz && !vert) {
                let leftSide = Math.min(coords[from][0], coords[to][0]);
                let rightSide = Math.max(coords[from][0], coords[to][0]);
                for(let i=leftSide; i<=rightSide; ++i) {
                    let y = parseInt(coords[from][1]);
                    // console.log(i, y);
                    data[i][y]++;
                    if(data[i][y] == 2) {
                        // console.log("Overlap at ", i, y);
                        sum++;
                    }
                }
            } else if (vert && !horiz) {
                let topSide = Math.min(coords[from][1], coords[to][1]);
                let botSide = Math.max(coords[from][1], coords[to][1]);
                for(let i=topSide; i<=botSide; ++i) {
                    let x = parseInt(coords[from][0]);
                    // console.log(x, i);
                    data[x][i]++;
                    if(data[x][i] == 2) {
                        // console.log("Overlap at ", x, i);
                        sum++;
                    }
                }
            } else if (!vert && !horiz) { // diagonal
                // console.log("Diagonal");
                let xdir = Math.sign(coords[to][0] - coords[from][0]);
                let ydir = Math.sign(coords[to][1] - coords[from][1]);
                for(let i=coords[from][0], j=coords[from][1]; i!=coords[to][0]+xdir; i+=xdir, j+=ydir) {
                    // console.log(i, j);
                    data[i][j]++;
                    if(data[i][j] == 2) {
                        // console.log("Overlap at ", i, j);
                        sum++;
                    }
                }
            }
        }
        return sum;
    }

    testB() {
        let ll = new LinkedList();
        ll.push({idx: 0, val: 1});
        ll.push({idx: 1, val: 2});
        ll.push({idx: 2, val: 3});
        ll.push({idx: 3, val: 4});
        ll.push({idx: 4, val: 5});
        console.log(ll.count());
        console.log(ll.find(el => el.idx == 4));
        ll.forEach(val => console.log(val));

        let mat = new SparseMatrix();
        mat.initialize({Count: 10, Value: 1 });
        console.log(mat.getValue(10, 10));
        mat.setValue(10, 10, { Count: 12, Value: 10 });
        console.log(mat.getValue(10, 10));
    }
}

export default new Day5TestClass();