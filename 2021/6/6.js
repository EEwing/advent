import fs from 'fs';
import TestClass from "../testclass.js"
import { LinkedList, SparseMatrix } from "../lib/math.js"

const from = 0;
const to = 1;

class LanternFish {
    constructor(currentTimer) {
        this.timerPeriod = 6;
        this.currentTimer = currentTimer;
    }
    tick() {
        if(this.currentTimer == 0) {
            this.currentTimer = this.timerPeriod;
            this.shouldReproduce = true;
        } else {
            this.currentTimer--;
        }
    }
    tickN(numTicks, depth=0) {
        let numChildren = this.numberOfOffSpring(numTicks);
        let sumTotalChildren = 0;
        for(let child=0; child<numChildren; ++child) {
            let newChild = new LanternFish(8);
            sumTotalChildren += newChild.tickN(numTicks - this.timerPeriod, depth+1);
        }
        // console.log("Fish depth", depth, "ticks:", numTicks, "children:", sumTotalChildren, "direct children:", numChildren)
        return sumTotalChildren + numChildren + 1;
    }
    numberOfOffSpring(numTicks) {
        let numChildren = Math.floor((numTicks - this.currentTimer)/this.timerPeriod);
        return numChildren;
    }
}

class Day5TestClass extends TestClass {
    constructor() {
        super();
        this.fish = [];
        this.readFish();
        this.childrenMap = [];
    }
    readFish() {
        this.fish = [];
        const rawInput = fs.readFileSync('6_input.txt', 'utf-8');
        let input = rawInput.split("\r\n");

        let initialFish = input[0].split(",");
        initialFish = initialFish.map(fish => parseInt(fish));

        for(let timer of initialFish) {
            this.fish.push(new LanternFish(timer));
        }
    }
    testA() {
        for(let i=0; i<80; ++i) {
            this.fish.forEach(fish => fish.tick());
            let reproducingFish = this.fish.filter(fish => fish.shouldReproduce);
            for(let fish of reproducingFish) {
                fish.shouldReproduce = false;
                this.fish.push(new LanternFish(8));
            }
        }
        return this.fish.length;
    }

    buildChildrenMap(ticksRemaining) {
        // let debug = ticksRemaining == 16;
        let numImmediateChildren = Math.floor(ticksRemaining/7);
        let sumChildren = numImmediateChildren;
        // if (debug) console.log(numImmediateChildren, sumChildren);
        for(let i=0; i<numImmediateChildren; ++i) {
            let childTicks = ticksRemaining - ((i+1)*7) - 2;
            // if (debug) console.log(i, childTicks);
            if(childTicks >= 2) {
                // console.log(`Child ${i} for fish with ${ticksRemaining} ticks remaining has ${childTicks} ticks to reproduce`);
                sumChildren += this.childrenMap[childTicks];
            }
        }
        this.childrenMap[ticksRemaining] = sumChildren;
    }
    
    testB() {
        this.readFish();
        this.childrenMap = []; // ticksRemaining

        let totalTicks = 256;

        for(let i=0; i<=totalTicks+6; ++i) {
            // console.log(`Building children map.... ${i}/${totalTicks}`)
            this.buildChildrenMap(i);
        }

        let sum = 0;
        this.fish.forEach(fish => {
            // console.log("Checking fish:", fish, totalTicks-fish.currentTimer+6);
            sum += this.childrenMap[totalTicks-fish.currentTimer+6] + 1;
            // console.log(sum);
        })
        console.log(this.childrenMap);
        return sum;
    }
}

export default new Day5TestClass();