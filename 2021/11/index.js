import fs from 'fs';
import TestClass from "../../testclass.js"

class Day11TestClass extends TestClass {
    constructor() {
        super();
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.input = rawInput.split("\r\n").map(val => val.split('').map(val => parseInt(val)));
        this.flashes = 0;
        this.kernel = [[1, 1, 1],
                        [1, 0, 1],
                        [1, 1, 1]];
        this.kernelOffset = [-1, -1];
    }
    getOctopus(x, y) {
        if(x < 0 || x >= this.input.length || y < 0 || y >= this.input[0].length) {
            return undefined;
        }
        return this.input[y][x];
    }
    step(stepNum) {
        let octopusFlashed = [];
        let octopusFlashing = [];
        // console.log("STEP", stepNum);
        for(let y=0; y<this.input.length; ++y) {
            let row = this.input[y];
            for(let x=0; x<row.length; ++x) {
                let octopus = row[x];
                //console.log(`${x}, ${y}:`, octopus);
                octopus++;
                if(octopus == 10) {
                    // console.log("Step", stepNum, "Octopus flashed at", x,",",y, ":");
                    octopusFlashing.push({x:x, y:y});
                }
                this.input[y][x] = octopus;
            }
        }
        while(octopusFlashing.length != 0) {
            let octopus = octopusFlashing.pop();
            // console.log(octopus);

            for(let y=0; y<this.kernel.length; ++y) {
                let row = this.kernel[y];
                for(let x=0; x<row.length; ++x) {
                    let xIdx = octopus.x + this.kernelOffset[0] + x;
                    let yIdx = octopus.y + this.kernelOffset[1] + y;
                    if(xIdx==octopus.x && yIdx==octopus.y) {
                        continue;
                    }
                    let neighbor = this.getOctopus(xIdx, yIdx);
                    // console.log("neighbor:", `${x}, ${y} => ${xIdx}, ${yIdx}: ${neighbor}`);
                    if(neighbor != undefined) {
                        neighbor+=this.kernel[y][x];
                        this.input[yIdx][xIdx] = neighbor;
                        if(neighbor == 10) {
                            // console.log("Step", stepNum, "Knock on flash at", xIdx,",",yIdx);
                            octopusFlashing.push({x:xIdx, y:yIdx});
                        }
                    }
                }
            }
        }
        let numFlashes = 0;
        for(let y=0; y<this.input.length; ++y) {
            let row = this.input[y];
            for(let x=0; x<row.length; ++x) {
                let octopus = row[x];
                if(octopus > 9) {
                    this.input[y][x] = 0;
                    numFlashes++;
                }
            }
        }
        return numFlashes;
    }
    testA() {
        let totalFlashes = 0;
        for(let i=0; i<100; ++i) {
            totalFlashes += this.step(i);
        }
        return totalFlashes;
    }
    testB() {
        let timeStep = 0;
        let numFlashes = 0;
        while(numFlashes != 100) {
            numFlashes = this.step(timeStep);
            timeStep++;
        }
        return timeStep;
    }
}

export default new Day11TestClass();