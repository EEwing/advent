import { count } from 'console';
import fs from 'fs';
import TestClass from "../../testclass.js"

function printMap(map) {
    console.log(map[0].map(val => "-").join('') + "--");
    for(let row of map) {
        let str = "|";
        for(let val of row) {
            str += val ? "█" : " ";
        }
        console.log(str+"|");
    }
    console.log(map[0].map(val => "-").join('') + "--");
}
function extractSubMap(oldMap, left, top, right, bottom) {
    left = Math.min(left, oldMap[0].length);
    right = Math.min(right, oldMap[0].length);
    top = Math.min(top, oldMap.length);
    bottom = Math.min(bottom, oldMap.length);

    // console.log("Extracting submap:", left, top, right, bottom);

    let newMap = [];
    for(let i=0; i<bottom-top; ++i) {
        newMap[i] = [];
        for (let j=0; j<right-left; ++j) {
            newMap[i][j] = oldMap[i+top][j+left];
        }
    }
    // console.log(newMap);
    return newMap;
}
function flipMap(map, yAxis) {
    let newMap = [];
    if(yAxis) {
        for(let i=0; i<map.length; ++i) {
            let newY = map.length - i - 1;
            newMap[newY] = [];
            for(let j=0; j<map[i].length; ++j) {
                newMap[newY][j] = map[i][j];
            }
        }
    } else {
        for(let i=0; i<map.length; ++i) {
            newMap[i] = [];
            for(let j=0; j<map[i].length; ++j) {
                let newX = map[i].length - j - 1;
                newMap[i][newX] = map[i][j];
            }
        }
    }
    return newMap;
}
function overlapMaps(map1, left1, top1, map2, left2, top2) {
    let newMap = [];
    let map1Height = map1.length;
    let map2Height = map2.length;
    let map1Width = map1[0].length;
    let map2Width = map2[0].length;
    let newHeight = Math.max(top1+map1Height, top2+map2Height);
    let newWidth = Math.max(left1+map1Width, left2+map2Width);
    for(let i=0; i<newHeight; ++i) {
        let map1Y = i-top1;
        let map2Y = i-top2;
        newMap[i] = [];
        for (let j=0; j<newWidth; ++j) {
            let map1X = j-left1;
            let map2X = j-left2;
            newMap[i][j] = false;
            if(map1Y >= 0 && map1X >= 0) {
                newMap[i][j] = map1[map1Y][map1X];
            }
            if(map2Y >= 0 && map2X >= 0) {
                newMap[i][j] = newMap[i][j] || map2[map2Y][map2X];
            }
        }
    }
    return newMap;
}
function countDots(map) {
    let sum = 0;
    for(let row of map) {
        for (let val of row) {
            if (val) sum++;
        }
    }
    return sum;
}

class Day13TestClass extends TestClass {
    constructor() {
        super();
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        let partInputs = rawInput.split('\r\n\r\n');
        this.dotsInput = partInputs[0].split('\r\n').map(row => row.split(',').map(val => parseInt(val)));
        this.foldsInput = partInputs[1].split('\r\n').map(row => [row.split('=')[0].charAt(row.split('=')[0].length-1), parseInt(row.split('=')[1])]);
    }
    buildDotsMap() {
        this.dotsMap = [];
        let maxX=0;
        let maxY=0;
        for(let row of this.dotsInput) {
            if(row[0] > maxX) {
                maxX = row[0]
            }
            if(row[1] > maxY) {
                maxY = row[1]
            }
        }
        for(let i=0; i<=maxY; ++i) {
            this.dotsMap[i] = [];
            for(let j=0; j<=maxX; ++j) {
                this.dotsMap[i][j] = false;
            }
        }
        for(let row of this.dotsInput) {
            this.dotsMap[row[1]][row[0]] = true;
        }
    }
    foldMap(oldMap, axis, location) {
        let oldHeight = oldMap.length;
        let oldWidth = oldMap[0].length;
        let newHeight = oldHeight;
        let newWidth = oldWidth;
        let map1Top, map1Left, map2Top, map2Left;
        let map1, map2;
        // console.log("full map:", countDots(oldMap));
        // printMap(oldMap);
        if(axis == 'y') {
            console.log("Folding vertical");
            map1 = extractSubMap(oldMap, 0, 0/*?*/, oldWidth, location);
            map2 = flipMap(extractSubMap(oldMap, 0, location+1, oldWidth, oldHeight), true);
        } else if(axis == 'x') {
            console.log("Folding horizontal");
            map1 = extractSubMap(oldMap, 0, 0, location, oldHeight);
            map2 = flipMap(extractSubMap(oldMap, location+1, 0, oldWidth, oldHeight), false);
        }
        // console.log("map1:", countDots(map1));
        // printMap(map1);
        // console.log("map2:", countDots(map2));
        // printMap(map2);
        map1Top = Math.max(0, map2.length - map1.length)
        map2Top = Math.max(0, map1.length - map2.length)

        map1Left = Math.max(0, map2[0].length - map1[0].length)
        map2Left = Math.max(0, map1[0].length - map2[0].length)
        let newMap = overlapMaps(map1, map1Left, map1Top, map2, map2Left, map2Top);
        // console.log("overlapped:", countDots(newMap));
        // printMap(newMap);
        return newMap;
    }
    testA() {
        this.buildDotsMap();
        let foldedMap = this.dotsMap;
        foldedMap = this.foldMap(foldedMap, this.foldsInput[0][0], this.foldsInput[0][1]);
        // let oneFold = this.foldMap(this.dotsMap, this.foldsInput[0][0], this.foldsInput[0][1]);
        let sum = countDots(foldedMap);
        return sum;
    }
    testB() {
        this.buildDotsMap();
        let foldedMap = this.dotsMap;
        for(let fold of this.foldsInput) {
            foldedMap = this.foldMap(foldedMap, fold[0], fold[1]);
            if(this.foldsInput.length - this.foldsInput.indexOf(fold) <= 3) {
                printMap(foldedMap);
            }
        }
        // let oneFold = this.foldMap(this.dotsMap, this.foldsInput[0][0], this.foldsInput[0][1]);
        let sum = countDots(foldedMap);
        return sum;
    }
}

export default new Day13TestClass();