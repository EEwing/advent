import fs from 'fs';

const data = fs.readFileSync('1a_input.txt', 'utf-8');
let lines = data.split("\n");

let lastLine = parseInt(lines[0]);
let numDepthIncreases = 0;
for(let i=1; i<lines.length; ++i) {
    let line = parseInt(lines[i])
    if(line > lastLine) {
        numDepthIncreases++;
    }
    lastLine = line
}
console.log(numDepthIncreases);