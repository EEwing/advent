import fs from 'fs';
import TestClass from "../../testclass.js"

function copyArray(arr) {
    return JSON.parse(JSON.stringify(arr));
}

class Day12TestClass extends TestClass {
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.input = rawInput.split("\r\n").map(row => row.split('-'));
        this.nodes = [];
    }
    getNode(nodeName) {
        for(let node of this.nodes) {
            if(node.name == nodeName) {
                return node;
            }
        }
    }
    buildMap() {
        this.nodeNames = [];
        for(let row of this.input) {
            if(!this.nodeNames.includes(row[0])) {
                this.nodeNames.push(row[0]);
            }
            if(!this.nodeNames.includes(row[1])) {
                this.nodeNames.push(row[1]);
            }
        }
        // Init paths
        this.paths = [[]];
        for(let i=0; i<this.nodeNames.length; ++i) {
            this.paths[i] = []
            for(let j=0; j<this.nodeNames.length; ++j) {
                this.paths[i][j] = false;
            }
        }
        // Calculate paths from input
        for(let row of this.input) {
            let fromIdx = this.nodeNames.indexOf(row[0]);
            let toIdx = this.nodeNames.indexOf(row[1]);
            this.paths[fromIdx][toIdx] = true;
            this.paths[toIdx][fromIdx] = true;
        }
    }
    getNumVisits(nodeName) {
        let numVisits = -1;
        if(nodeName == "start") {
            numVisits = 0;
        } else if(nodeName == "end") {
            numVisits = 1;
        } else if (nodeName.toLowerCase() == nodeName) {
            numVisits = 2;
        }
        return numVisits
    }
    buildMapv2() {
        this.nodeNames = [];
        for(let row of this.input) {
            if(!this.nodeNames.includes(row[0])) {
                this.nodeNames.push(row[0]);
            }
            if(!this.nodeNames.includes(row[1])) {
                this.nodeNames.push(row[1]);
            }
        }
        // Init paths
        this.paths = [[]];
        for(let i=0; i<this.nodeNames.length; ++i) {
            this.paths[i] = []
            for(let j=0; j<this.nodeNames.length; ++j) {
                this.paths[i][j] = 0;
            }
        }
        // Calculate paths from input
        for(let row of this.input) {
            let fromIdx = this.nodeNames.indexOf(row[0]);
            let toIdx = this.nodeNames.indexOf(row[1]);
            let fromVisits = this.getNumVisits(row[0]);
            let toVisits = this.getNumVisits(row[1]);
            this.paths[fromIdx][toIdx] = toVisits;
            this.paths[toIdx][fromIdx] = fromVisits;
        }
    }
    calculateAllPaths(nodeName, paths, currentRoute=["start"]) {
        // console.log("Checking next node for route", currentRoute, "and node", nodeName);
        let fromIdx = this.nodeNames.indexOf(nodeName);

        if(nodeName == "end") {
            return [currentRoute];
        }

        // Remove self from paths if needed
        let newPaths = copyArray(paths);
        if(nodeName.toLowerCase() == nodeName) {
            let row = newPaths[fromIdx];
            for(let i=0; i<row.length; ++i) {
                row[i] = false; // Clear out outgoing paths
            }
            for(let i=0; i<newPaths.length; ++i) {
                newPaths[i][fromIdx] = false; // Clear out incoming paths
            }

        }

        let pathsAway = paths[fromIdx];
        let routes = [];
        for(let i=0; i<pathsAway.length; ++i) {
            let pathFound = pathsAway[i];
            if(pathFound) {
                let toNodeName = this.nodeNames[i];
                let nextRoute = copyArray(currentRoute);
                nextRoute.push(toNodeName);
                let finishedRoutes = this.calculateAllPaths(toNodeName, newPaths, nextRoute);
                // console.log("Got routes:", finishedRoutes);
                routes = routes.concat(finishedRoutes);
            }
        }
        // console.log("End:", paths, routes);
        return routes;
    }
    testA() {
        this.buildMap();
        let paths = this.calculateAllPaths("start", copyArray(this.paths));
        return paths.length;
    }
    calculateAllPathsv2(nodeName, paths, currentRoute=["start"], blockSmallCaves=false) {
        // console.log("Checking next node for route", currentRoute, "and node", nodeName);
        let fromIdx = this.nodeNames.indexOf(nodeName);

        // console.log(currentRoute.join(","));
        if(nodeName == "end") {
            return [currentRoute];
        }

        // Remove self from paths if needed
        let newPaths = copyArray(paths);
        let visitsRemaining = 0;
        if(nodeName.toLowerCase() == nodeName) {
            for(let i=0; i<newPaths.length; ++i) {
                if(newPaths[i][fromIdx] > 0) {
                    newPaths[i][fromIdx] -= 1; // Reduce number of incoming routes left
                    visitsRemaining = newPaths[i][fromIdx];
                }
            }
        }

        // If this is a small cave, no other small cave can be visited again
        if(this.getNumVisits(nodeName) == 2 && visitsRemaining == 0 && !blockSmallCaves) {
            // console.log(`Entered small cave "${nodeName}" twice! Can only visit other caves once now.`);
            blockSmallCaves = true;
            for(let otherNode of this.nodeNames) {
                let nodeIdx = this.nodeNames.indexOf(otherNode);
                if(this.getNumVisits(otherNode) == 2) {
                    // console.log("Reducing visits for node", otherNode);
                    for(let i=0; i<newPaths.length; ++i) {
                        if(newPaths[i][nodeIdx] > 0) {
                            newPaths[i][nodeIdx] -= 1;
                        }
                    }
                } 
            }
        }

        let pathsAway = newPaths[fromIdx];
        let routes = [];
        for(let i=0; i<pathsAway.length; ++i) {
            let pathFound = pathsAway[i] >0 || pathsAway[i] == -1;
            if(pathFound) {
                let toNodeName = this.nodeNames[i];
                let nextRoute = copyArray(currentRoute);
                nextRoute.push(toNodeName);
                let finishedRoutes = this.calculateAllPathsv2(toNodeName, newPaths, nextRoute, blockSmallCaves);
                // console.log("Got routes:", finishedRoutes);
                routes = routes.concat(finishedRoutes);
            }
        }
        // console.log(currentRoute.join(','), "<---");
        return routes;
    }
    testB() {
        this.buildMapv2();
        let paths = this.calculateAllPathsv2("start", copyArray(this.paths));
        // console.log("Routes:", paths.map(path => path.join(',')).join('\n'));
        return paths.length;
    }
}

export default new Day12TestClass();