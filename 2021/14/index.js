import fs from 'fs';
import TestClass from "../../testclass.js"

function matMultiply(mat, vec) {
    if(mat[0].length != vec.length) {
        return undefined;
    }
    let returnVec = [];
    for(let i=0; i<mat.length; ++i) {
        let sum = 0;
        let row = mat[i];
        for(let j=0; j<row.length; ++j) {
            sum += mat[i][j]*vec[j]
        }
        returnVec[i] = sum;
    }
    return returnVec;
}

class Day14TestClass extends TestClass {
    constructor() {
        super();
        this.lines = [];
    }
    init() {
        const rawInput = fs.readFileSync('example.txt', 'utf-8');
        this.lines = rawInput.split('\r\n');
        this.polymerTemplate = this.lines.shift();
        this.lines.shift();
        this.pairs = [];
        for(let row of this.lines) {
            let pair = row.split(" -> ");
            this.pairs[pair[0]] = pair[1];
        }
        // this.pairs = this.lines.map(row => row.split(" -> "));
    }
    polymerize(polymer) {
        let newPolymer = polymer[0];
        for(let i=0; i<polymer.length-1; ++i) {
            let group = polymer[i] + polymer[i+1];
            let insertion = this.pairs[group];
            if(insertion != undefined) {
                newPolymer += insertion;
            }
            newPolymer += polymer[i+1];
        }
        return newPolymer;
    }
    testA() {
        console.log(this.polymerTemplate, this.pairs);
        let polymer = this.polymerTemplate;
        for(let i=0; i<10; ++i) {
            polymer = this.polymerize(polymer);
        }
        let counts = [];
        for (let ch of polymer) {
            let chFound = false;
            for(let val of counts) {
                if(val.char == ch) {
                    chFound = true;
                    val.count += 1;
                }
            }
            if(!chFound) {
                counts.push({char: ch, count: 1});
            }
        }
        counts = counts.sort((a, b) => b.count - a.count);
        return counts.shift().count - counts.pop().count;
    }
    testB() {
        let pairsArray = Object.keys(this.pairs);
        let numPairs = pairsArray.length;
        let mat = [[]];
        for(let i=0; i<numPairs; ++i) {
            mat[i] = [];
            for(let j=0; j<numPairs; ++j) {
                mat[i][j] = 0;
            }
        }
        for(let i=0; i<numPairs; ++i) {
            let newPolymer = this.polymerize(pairsArray[i]);

            let polymer1 = newPolymer[0] + newPolymer[1];
            mat[pairsArray.indexOf(polymer1)][i] = 1;

            if(newPolymer.length == 3) {
                let polymer2 = newPolymer[1] + newPolymer[2];
                mat[pairsArray.indexOf(polymer2)][i] = 1;
            }
        }
        let polymer = [];
        for(let i=0; i<pairsArray.length; ++i) {
            polymer[i] = 0;
        }
        for(let i=0; i<this.polymerTemplate.length-1; ++i) {
            let pair = this.polymerTemplate[i] + this.polymerTemplate[i+1];
            polymer[pairsArray.indexOf(pair)] += 1;
        }
        for(let i=0; i<40; ++i) {
            polymer = matMultiply(mat, polymer);
        }
        console.log(pairsArray);
        console.log(polymer);

        // Count up polymer vector
        let counts = [];
        for(let i=0; i<numPairs; ++i) {
            let polyCount = polymer[i];
            let pair = pairsArray[i];
            for(let ch of pair) {
                let chFound = false;
                for(let val of counts) {
                    if(val.char == ch) {
                        chFound = true;
                        val.count += polyCount;
                    }
                }
                if(!chFound) {
                    counts.push({char: ch, count: polyCount});
                }
            }
        }
        for(let val of counts) {
            if(val.char == this.polymerTemplate[0] || val.char == this.polymerTemplate[this.polymerTemplate.length-1]) {
                val.count += 1;
            }
        }
        for(let val of counts) {
           val.count /= 2;
        }
        console.log(counts);

        counts = counts.sort((a, b) => b.count - a.count);
        return counts.shift().count - counts.pop().count;
    }
}

export default new Day14TestClass();