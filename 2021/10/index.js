import fs from 'fs';
import TestClass from "../../testclass.js"
import { LinkedList, SparseMatrix } from "../../lib/math.js"
class Day10TestClass extends TestClass {
    constructor() {
        super();
        this.matching = {
            '[': ']',
            '(': ')',
            '{': '}',
            '<': '>',
        }
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.input = rawInput.split("\r\n");
    }
    parseLine(line) {
        let pushes = ['[', '(', '{', '<'];
        let pops = [']', ')', '}', '>'];
        let stack = [];
        let invalidChar = undefined;
        for(let char of line) {
            if(pushes.includes(char)) {
                stack.push(char);
            } else if (pops.includes(char)) {
                let prevChar = stack.pop();
                if(prevChar == undefined || this.matching[prevChar] != char) {
                    invalidChar = char;
                    break;
                }
            } else {
                console.log("INVALID CHAR", char);
            }
        }
        return {invalidChar, stack};
    }
    testA() {
        let scores = {
            ')': 3,
            ']': 57,
            '}': 1197,
            '>': 25137,
        }
        let sum = 0;
        for(let line of this.input) {
            let {invalidChar, stack} = this.parseLine(line);
            if(invalidChar != undefined) {
                sum += scores[invalidChar];
            }
        }
        return sum;
    }
    testB() {
        let scores = {
            '(': 1,
            '[': 2,
            '{': 3,
            '<': 4
        }
        let lineScores = [];
        for(let line of this.input) {
            let {invalidChar, stack} = this.parseLine(line);
            if(invalidChar == undefined) {
                let score = 0;
                while(stack.length > 0) {
                    let char = stack.pop();
                    score *= 5;
                    score += scores[char];
                }
                lineScores.push(score);
            }
        }
        lineScores.sort((a,b) => b-a);
        return lineScores[Math.floor(lineScores.length/2)];
    }
}

export default new Day10TestClass();