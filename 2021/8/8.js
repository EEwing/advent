import fs from 'fs';
import TestClass from "../testclass.js"
import { LinkedList, SparseMatrix } from "../lib/math.js"

function arrSubtract(arr1, arr2, twoWay=false) {
    let newArr = arr1.filter(val => arr2.indexOf(val) == -1);
    if(twoWay) {
        newArr = newArr.concat(arr2.filter(val => arr1.indexOf(val) == -1));
    }
    return newArr;
}
function arrIncludesChars(input, chars) {
    let includesChars = true;
    for(let ch of chars) {
        // console.log("pre:", ch, filteredInput);
        includesChars &= input.includes(ch);
        // console.log("post:", ch, filteredInput);
    }
    return includesChars;
}

class Day5TestClass extends TestClass {
    constructor() {
        super();
        this.countMap = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6];
        this.segments = []; // top, topleft, topright, middle, botleft, botright, bottom
        this.numberMap = [
            [0, 1, 2, 4, 5, 6],
            [2, 5],
            [0, 2, 3, 4, 6],
            [0, 2, 3, 5, 6],
            [1, 2, 3, 5],
            [0, 1, 3, 5, 6],
            [0, 1, 3, 4, 5, 6],
            [0, 2, 5],
            [0, 1, 2, 3, 4, 5, 6],
            [0, 1, 2, 3, 5, 6],
        ]
    }
    init() {
        const rawInput = fs.readFileSync('8_input.txt', 'utf-8');
        this.input = rawInput.split("\r\n").map(line => line.split(" | "));

        this.inputs = this.input.map(row => row[0].split(" "));
        this.outputs = this.input.map(row => row[1].split(" "));

        this.initSegments();
    }
    initSegments() {
        for(let i=0; i<10; ++i) {
            this.segments[i] = {
                segmentIdx: i,
                possibleChars: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                locked: false,
                lockedValue: undefined
            };
        }
    }
    get1478(rowNum) {
        let sum = 0;
        for(let chars of this.outputs[rowNum]) {
            if([2, 3, 4, 7].includes(chars.length)) {
                sum++;
            }
        }
        return sum;
    }
    testA() {
        // 2, 4, 3, 7
        let numChars = 0;
        for(let i=0; i<this.outputs.length; ++i) {
            numChars += this.get1478(i);
        }
        return numChars;
    }
    restrict(segment, chars) {
        let segmentData = this.segments[segment];
        segmentData.possibleChars = segmentData.possibleChars.filter(ch => chars.includes(ch));
        if(segmentData.possibleChars == 1) {
            this.lockSegment(segment, segmentData.possibleChars[0]);
        } else if (segmentData.possibleChars < 1) {
            console.log("Out-logic'd ourselves!", this.segments, segmentData, segment, chars);
            process.exit(-1);
        }
        console.log("Segment", segment, "new restriction:", segmentData.possibleChars.join(""));
        this.segments[segment] = segmentData;
    }
    lockSegment(segment, char) {
        console.log("Locking segment", segment, "to char", char);
        let segmentData = this.segments[segment];
        segmentData.locked = true;
        segmentData.lockedValue = char;
        segmentData.possibleChars = [char];
        this.segments[segment] = segmentData;
    }
    getSegmentChar(segmentIdx) {
        return ['a', 'b', 'c', 'd', 'e', 'f', 'g'][segmentIdx];
    }
    calculateLogic(given, inputs, maxDepth=1) {
        console.log("Calculating logic:", given, inputs);
        let lockedSegments = given.filter(segment => segment.locked);

        console.log("Locked letters:", lockedSegments);

        // Remove locked segments from the numbers
        let filteredInputs = [];
        for(let inp of inputs) {
            console.log(inp, lockedSegments.map(seg => seg.lockedValue));
            let filteredInp = arrSubtract(inp.split(''), lockedSegments.map(seg => seg.lockedValue), true);
            filteredInputs.push(filteredInp.join(''));
        }
        console.log("Filtered inputs:", filteredInputs);

        // Calculate which inputs are unique
        let newRestrictions = [];
        for(let i=0; i<10; ++i) {
            let segments = this.numberMap[i];
            let filteredSegments = this.numberMap[i].filter(val => lockedSegments.find(seg => seg.segmentIdx == val) == undefined);
            newRestrictions[i] = { segmentIdx: i, possibleSegments: filteredSegments };

        }
        let numCounts = []
        for(let number of newRestrictions) {
            let count = number.possibleSegments.length;
            let sum = 1
            if(numCounts[count] != undefined) {
                sum = numCounts[count] + 1;
            } 
            numCounts[count] = sum;
        }
        let uniqueLengths = newRestrictions.filter(num => numCounts[num.possibleSegments.length] == 1);
        uniqueLengths.sort((a, b) => {
            return a.possibleSegments.length - b.possibleSegments.length;
        });

        console.log("unique lengths:", uniqueLengths);

        // Calculate some tighter restrictions
        for(let segment of uniqueLengths) {
            let matchingInputs = inputs.filter(input => segment.possibleSegments.length == input.length);
            for(let inp of matchingInputs) {
                this.restrict(segment.segmentIdx, inp.split(''));
            }
        }

        // Calculate knock-on logic from those restrictions
        for(let segment of this.segments) {
            let thisNumberMap = this.numberMap[segment.segmentIdx];
            if(segment.possibleChars.length == thisNumberMap.length) {
                for(let i=0; i<this.numberMap.length; ++i) {
                    if(i==segment.segmentIdx) continue;
                    let diff = arrSubtract(thisNumberMap, this.numberMap[i]).concat(arrSubtract(this.numberMap[i], thisNumberMap));
                    let diffChars = arrSubtract(segment.possibleChars, this.segments[i].possibleChars).concat(arrSubtract(this.segments[i].possibleChars, segment.possibleChars));
                    if(diff.length == 1 && segment.possibleChars.length > this.segments[i].possibleChars.length) {
                        let diffIdx = diff[0];
                        console.log("Checking", segment.segmentIdx, "vs", i, diff, diffChars);
                        this.lockSegment(diff[0], diffChars[0]);
                    }
                }
            }
        }

        // Recurse.
        if(maxDepth != 0) {
            this.calculateLogic(this.segments, filteredInputs, maxDepth-1);
        }

        // console.log("segments", this.segments);
    }
    testB() {
        // top == 0 & 2 & 3 & 5 & 6 & 7 & 8 & 9
        // topleft == 0 & 4 & 5 & 6 & 8 & 9
        // topright == 0 & 1 & 2 & 3 & 4 & 7 & 8 & 9
        // mid == 2 & 3 & 4 & 5 & 6 & 8 & 9
        // botleft == 0 & 2 & 6 & 8
        // botright == 0 & 1 & 3 & 4 & 5 & 6 & 7 & 8 & 9
        // bot == 0 & 2 & 3 & 5 & 6 & 8 & 9
        /*
        for(let i=0; i<this.inputs.length; ++i) {
            this.initSegments();
            this.calculateLogic(this.segments, this.inputs[i]);
            console.log(this.segments);
        }
        */
        
        let sum = 0;
        for(let i=0; i<this.inputs.length; ++i) {
            console.log("testing input", i);
            this.initSegments();
            let input = this.inputs[i]

            let inp1 = input.filter(val => val.length == 2)[0];
            let inp4 = input.filter(val => val.length == 4)[0];
            let inp7 = input.filter(val => val.length == 3)[0];
            let inp8 = input.filter(val => val.length == 7)[0];

            let segments = [];
            // 1 logic
            segments[2] = inp1.split('');
            segments[5] = segments[2];

            // 1x7 logic
            segments[0] = arrSubtract(inp7.split(''), inp1.split(''), true).join('');

            // 1x4 logic
            segments[1] = arrSubtract(inp4.split(''), inp1.split(''), true);
            segments[3] = segments[1];

            let inp3 = input.filter(val => val.length == 5 && arrIncludesChars(val, segments[0] + segments[2][0]+segments[2][1]))[0];
            console.log("input3:", inp3);

            // 3x7 logic
            if(inp3.split('').includes(segments[3][0])) {
                segments[1] = segments[3][1];
                segments[3] = segments[3][0];
            } else {
                segments[1] = segments[3][0];
                segments[3] = segments[3][1];
            }

            let diff38 = arrSubtract(inp3.split(''), inp8.split(''), true);
            segments[4] = arrSubtract(diff38, segments[1])[0];
            segments[6] = arrSubtract(inp8.split(''), segments[0]+segments[1]+segments[2]+segments[3]+segments[4]+segments[5])[0];

            let inp6 = input.filter(val => val.length == 6 && arrIncludesChars(val, segments[0] + segments[1]+segments[3]+segments[4]+segments[6]))[0];
            // 6x1 logic
            if(inp6.split('').includes(segments[2][0])) {
                segments[5] = segments[2][0];
                segments[2] = segments[2][1];
            } else {
                segments[5] = segments[2][1];
                segments[2] = segments[2][0];
            }
            // Locked in 0, 2, 5

            let numbers = [];
            for(let num of this.numberMap) {
                numbers.push(num.map(idx=>segments[idx]).sort().join(''));
            }

            let output = this.outputs[i];
            let sortedOutputs = output.map(out => out.split('').sort().join(''));
            let outputNum = '';
            for(let out of sortedOutputs) {
                let num = numbers.indexOf(out);
                if(num == -1) {
                    console.log("Could not find number for", out, "in", numbers, segments);
                    continue;
                }
                outputNum += numbers.indexOf(out);
                // console.log(out, numbers.indexOf(out));
            }
            console.log(outputNum);
            sum += parseInt(outputNum);
        }
        return sum;
    }
}

export default new Day5TestClass();