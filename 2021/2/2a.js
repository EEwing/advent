import fs from 'fs';

const data = fs.readFileSync('2_input.txt', 'utf-8');
let lines = data.split("\n");

let pos = 0;
let depth = 0;

for(let i=0; i<lines.length; ++i) {
    let args = lines[i].split(" ");
    switch (args[0]) {
        case "forward":
            pos += parseInt(args[1])
            break;
        case "down":
            depth += parseInt(args[1])
            break;
        case "up":
            depth -= parseInt(args[1])
            break;
    }
}

console.log(pos * depth);