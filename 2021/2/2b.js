import fs from 'fs';

const data = fs.readFileSync('2_input.txt', 'utf-8');
let lines = data.split("\n");

let aim = 0;
let pos = 0;
let depth = 0;

for(let i=0; i<lines.length; ++i) {
    let args = lines[i].split(" ");
    let val = parseInt(args[1])
    switch (args[0]) {
        case "forward":
            pos += val;
            depth += aim * val;
            break;
        case "down":
            aim += val;
            break;
        case "up":
            aim -= val;
            break;
    }
}

console.log(pos * depth);