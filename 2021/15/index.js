import fs from 'fs';
import { runInContext } from 'vm';
import TestClass from "../../testclass.js"

function printMap(map, padding=4) {
    console.log("-".repeat(padding*map[0].length + 2));
    for(let row of map) {
        let rowStr = "|";
        for(let col of row) {
            rowStr += col.toString().padEnd(padding);
        }
        console.log(rowStr + "|");
    }
    console.log("-".repeat(padding*map[0].length + 2));
}

function dumpMap(map, delimiter, fileName) {
    let mapStr = map.map(row => row.join(delimiter)).join("\n");
    fs.writeFileSync(fileName, mapStr);
}

class Day14TestClass extends TestClass {
    constructor() {
        super();
        this.lines = [];
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.lines = rawInput.split('\r\n');
        this.map = this.lines.map(row => row.split('').map(val => parseInt(val)));
    }
    getNode(x, y) {
        if(x>=0 && y>=0 && y<this.map.length && x<this.map[0].length) {
            return {x: x, y: y, risk: this.map[y][x]};
        }
        return undefined;
    }
    getMinRiskRoute(riskMap) {
        let startingNode = this.getNode(0, 0);
        let endingNode = this.getNode(riskMap[0].length-1, riskMap.length-1);
        let curNode = endingNode;
        let path = [endingNode];
        while(curNode.x != startingNode.x || curNode.y != startingNode.y) {
            let neighbors = [[-1, 0], [0, -1]];
            let minRiskNeighbor = undefined;
            for(let neighbor of neighbors) {
                let neighborNode = this.getNode(curNode.x + neighbor[0], curNode.y + neighbor[1]);
                if(neighborNode == undefined) {
                    continue;
                }
                let riskAmount = riskMap[neighborNode.y][neighborNode.x];
                neighborNode.totalRisk = riskAmount;
                if(minRiskNeighbor == undefined || minRiskNeighbor.totalRisk > neighborNode.totalRisk) {
                    minRiskNeighbor = neighborNode;
                }
            }
            if(minRiskNeighbor == undefined) {
                console.log("HALP", curNode, endingNode, path);
            }
            curNode = minRiskNeighbor;
            path.push(curNode);
        }
        return path;
    }
    testA() {
        let riskMap = this.map.map(row => row.map(col => NaN)); // Same size map, but all invalid
        let startingNode = this.getNode(0, 0);
        // riskMap[0][0] = startingNode.risk;
        riskMap[0][0] = 0; // Starting node has no risk
        let nodesToSearch = [startingNode];
        // printMap(riskMap);
        while (nodesToSearch.length > 0) {
            let node = nodesToSearch.shift();
            let curRisk = riskMap[node.y][node.x];
            let neighbors = [[1, 0], [0, 1], [-1, 0], [0, -1]];
            for(let neighbor of neighbors) {
                let neighborNode = this.getNode(node.x + neighbor[0], node.y + neighbor[1]);
                if(neighborNode == undefined) {
                    continue;
                }
                let riskAmount = riskMap[neighborNode.y][neighborNode.x];
                if(isNaN(riskAmount) || riskAmount > curRisk + neighborNode.risk) {
                    riskMap[neighborNode.y][neighborNode.x] = curRisk + neighborNode.risk;
                    if(!nodesToSearch.includes(neighborNode)) {
                        nodesToSearch.push(neighborNode);
                    }
                }
            }
        }
        // printMap(riskMap, 3);
        let path = this.getMinRiskRoute(riskMap);
        let pathMap = riskMap.map(row => row.map(col => " "));
        for(let node of path) {
            pathMap[node.y][node.x] = "███";
        }
        // printMap(pathMap, 3);
        let endingRisk = riskMap[this.map.length-1][this.map[0].length-1];
        return endingRisk;
    }
    testB() {
        // Make a huge map first
        let mapCopy = [[]];
        for(let i=0; i<5; ++i) {
            for(let y=0; y<this.map.length; ++y) {
                mapCopy[y+i*this.map.length] = [];
            }
            for(let j=0; j<5; ++j) {
                for(let y=0; y<this.map.length; ++y) {
                    let yIdx = this.map.length * i + y;
                    let row = this.map[y];
                    for (let x=0; x<row.length; ++x) {
                        let xIdx = row.length * j + x;
                        mapCopy[yIdx][xIdx] = (this.map[y][x] + i + j);
                        if(mapCopy[yIdx][xIdx] >= 10) {
                            mapCopy[yIdx][xIdx] -= 9;
                        }
                    }
                }
            }
        }
        this.map = mapCopy;
        dumpMap(this.map, "", "out.csv");


        let riskMap = this.map.map(row => row.map(col => NaN)); // Same size map, but all invalid
        let startingNode = this.getNode(0, 0);
        // riskMap[0][0] = startingNode.risk;
        riskMap[0][0] = 0; // Starting node has no risk
        let nodesToSearch = [startingNode];
        // printMap(riskMap);
        while (nodesToSearch.length > 0) {
            let node = nodesToSearch.shift();
            let curRisk = riskMap[node.y][node.x];
            let neighbors = [[1, 0], [0, 1], [-1, 0], [0, -1]];
            for(let neighbor of neighbors) {
                let neighborNode = this.getNode(node.x + neighbor[0], node.y + neighbor[1]);
                if(neighborNode == undefined) {
                    continue;
                }
                let riskAmount = riskMap[neighborNode.y][neighborNode.x];
                if(isNaN(riskAmount) || riskAmount > curRisk + neighborNode.risk) {
                    riskMap[neighborNode.y][neighborNode.x] = curRisk + neighborNode.risk;
                    if(!nodesToSearch.includes(neighborNode)) {
                        nodesToSearch.push(neighborNode);
                    }
                }
            }
        }
        dumpMap(riskMap, "", "risk.txt");
        // printMap(riskMap, 4);
        let path = this.getMinRiskRoute(riskMap);
        let pathMap = riskMap.map(row => row.map(col => " "));
        for(let node of path) {
            pathMap[node.y][node.x] = "████";
        }
        // printMap(pathMap, 4);
        let endingRisk = riskMap[this.map.length-1][this.map[0].length-1];
        return endingRisk;
    }
}

export default new Day14TestClass();