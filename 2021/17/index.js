import fs, { symlinkSync } from 'fs';
import TestClass from "../../testclass.js"

function printMap(map, padding=4) {
    console.log("-".repeat(padding*map[0].length + 2));
    for(let row of map) {
        let rowStr = "|";
        for(let col of row) {
            rowStr += col.toString().padEnd(padding);
        }
        console.log(rowStr + "|");
    }
    console.log("-".repeat(padding*map[0].length + 2));
}

function dumpMap(map, delimiter, fileName) {
    let mapStr = map.map(row => row.join(delimiter)).join("\n");
    fs.writeFileSync(fileName, mapStr);
}

function addTowardZero(val, inc) {
    if(val < 0) {
        return Math.min(val + inc, 0);
    } else if (val > 0) {
        return Math.max(val - inc, 0);
    } else {
        console.log("Adding to zero!", inc);
        return val;
    }
}

function sumDown(val) {
    return (val * (val+1))/2;
}

class Day17TestClass extends TestClass {
    constructor() {
        super();
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.input = rawInput.split('\r\n')[0];
        let parsedInput = this.input.split(",");
        let xRange = parsedInput[0].split("x=").pop();
        this.xMin = parseInt(xRange.split("..")[0]);
        this.xMax = parseInt(xRange.split("..")[1]);
        let yRange = parsedInput[1].split("y=").pop();
        this.yMin = parseInt(yRange.split("..")[0]);
        this.yMax = parseInt(yRange.split("..")[1]);

    }
    testA() {
        console.log(this);
        let yVel = -this.yMin - 1;
        let sum = 0;
        for(let i=0; i<=yVel; ++i) {
            sum += i;
        }
        return sum;
    }
    testB() {
        let count = 0;
        let allVelocities = [];
        console.log(`${this.xMin} -> ${this.xMax}, ${this.yMin} -> ${this.yMax}`);
        for(let x=this.xMin; x<=this.xMax; ++x) {
            let possibleXVelocities = [{val: x, steps: 1, xVel: x}];
            let xVel = addTowardZero(x, -1)/2;
            if(xVel%1 != 0) {
                // Skip this value if it ends in .5
                xVel = addTowardZero(xVel, 0.5);
            } 
            while(sumDown(xVel) >= x) {
                let numSteps = 1;
                let xPos = xVel; // Start from first step
                let curVel = addTowardZero(xVel, 1);
                while (xPos < x) {
                    numSteps += 1;
                    xPos += curVel;
                    curVel = addTowardZero(curVel, 1);
                }

                if(xPos == x) {
                    possibleXVelocities.push({val: xVel, steps: numSteps, xVel: curVel});

                    // let numSteps = 0; // val = n + (n+1) + (n+2)
                    // val = n ( 1+2+3 )
                    // n = val / (1+2+3+ ... )
                    // n = val / (n*(n+1)/2)
                    // n^

                }

                // console.log(x, xVel);
                xVel = addTowardZero(xVel, 1);
            }

            let possibleVelocities = [];
            for(let xOption of possibleXVelocities) {
                let xVal = xOption.val*xOption.steps - sumDown(xOption.steps - 1);
                // console.log("Checking for y value with x velocity", xOption.val, "in", xOption.steps, "steps");
                for(let startingYPos=this.yMin; startingYPos<=this.yMax; ++startingYPos) {
                    let minY = Math.min(startingYPos, 0);
                    let maxY = Math.abs(startingYPos);
                    // console.log("Checking from", `${minY} to ${maxY} for ${x},${startingYPos}`);
                    for(let y=minY; y<=maxY; ++y) {
                        // console.log(`Checking starting velocity ${xOption.val},${y} to get to point ${xVal},${startingYPos}`);
                        let yVal = y*xOption.steps - sumDown(xOption.steps-1);
                        let velObj = {x: xOption.val, y: y};
                        if(yVal == startingYPos) {
                            // console.log("Velocity", `${xOption.val},${y} ends at point ${xVal},${startingYPos}} after ${xOption.steps} steps`);
                            possibleVelocities.push(velObj);
                        } else if (yVal > startingYPos && xOption.xVel == 0) {
                            // Might be a few more steps straight down
                            let curVel = y - xOption.steps;
                            // console.log("--Might still be straight down.", `Ended at ${xVal},${yVal} with yVel ${curVel}`);
                            while(yVal > startingYPos) {
                                yVal += curVel;
                                curVel -= 1;
                            }
                            if(yVal == startingYPos) {
                                // console.log("--Fell straight down with:", `${xOption.val},${y}`)
                                possibleVelocities.push(velObj);
                            }
                        }
                    }
                }
            }
            // console.log(possibleVelocities);

            // Intersection
            for(let vel of possibleVelocities) {
                let found = false;
                for(let otherVel of allVelocities) {
                    found |= (otherVel.x == vel.x && otherVel.y == vel.y);
                }
                if(!found) {
                    allVelocities.push(vel);
                }
            }
        }
        // console.log(allVelocities);
        return allVelocities.length;
    }
}

export default new Day17TestClass();