import fs from 'fs';
import TestClass from "../testclass.js"
import { LinkedList, SparseMatrix } from "../lib/math.js"

class Day9TestClass extends TestClass {
    constructor() {
        super();
    }
    init() {
        const rawInput = fs.readFileSync('9_input.txt', 'utf-8');
        this.input = rawInput.split("\r\n");
        this.map = this.input.map(row => row.split("").map(val => parseInt(val)));
        this.pointMap = [];
    }
    testA() {
        let lowPoints = [];
        for(let i=0; i<this.map.length; ++i) {
            let row = this.map[i];
            for(let j=0; j<row.length; ++j) {
                let height = row[j];
                let isLowPoint = true;
                // Left
                if(j>0) {
                    isLowPoint &= row[j-1] > row[j];
                }
                // Right
                if(j<row.length-1) {
                    isLowPoint &= row[j+1] > row[j];
                }
                // Up
                if(i>0) {
                    isLowPoint &= this.map[i-1][j] > row[j];
                }
                // Down
                if(i<this.map.length-1) {
                    isLowPoint &= this.map[i+1][j] > row[j];
                }
                if(isLowPoint){
                    lowPoints.push(height);
                }
            }
        }
        let danger = 0;
        for(let height of lowPoints) {
            danger += height +1;
        }
        return danger;
    }
    calculateBasin(initialPoint) {
        let pointsToSearch = [initialPoint];
        let includedPoints = new Set(), visitedPoints = new Set();
        while(pointsToSearch.length>0) {
            // console.log("Checking", pointsToSearch.length, "points");
            // console.log("searching:", pointsToSearch);
            // console.log("included:", includedPoints);
            // console.log("visited:", visitedPoints);
            let point = pointsToSearch.pop();
            visitedPoints.add(point);

            let height = point.height;
            if(height >= 9) {
                continue;
            }

            includedPoints.add(point);

            let row = this.map[point.y];

            // console.log("point:", point);

            // Left
            if(point.x>0) {
                // console.log(this.pointMap, point.x, this.pointMap[point.x-1]);
                let leftPoint = this.pointMap[point.y][point.x-1];
                // console.log("left:", leftPoint, visitedPoints.has(leftPoint), pointsToSearch.includes(leftPoint));
                if(!visitedPoints.has(leftPoint) && !pointsToSearch.includes(leftPoint)) {
                    pointsToSearch.push(leftPoint);
                }
            }
            // Right
            if(point.x<row.length-1) {
                let rightPoint = this.pointMap[point.y][point.x+1];
                // console.log("right:", rightPoint, visitedPoints.has(rightPoint), pointsToSearch.includes(rightPoint));
                if(!visitedPoints.has(rightPoint) && !pointsToSearch.includes(rightPoint)) {
                    pointsToSearch.push(rightPoint);
                }
            }
            // Up
            if(point.y>0) {
                let upPoint = this.pointMap[point.y-1][point.x];
                // console.log("up:", upPoint, visitedPoints.has(upPoint), pointsToSearch.includes(upPoint));
                if(!visitedPoints.has(upPoint) && !pointsToSearch.includes(upPoint)) {
                    pointsToSearch.push(upPoint);
                }
            }
            // Down
            if(point.y<this.map.length-1) {
                let downPoint = this.pointMap[point.y+1][point.x];
                // console.log("down:", downPoint, visitedPoints.has(downPoint), pointsToSearch.includes(downPoint));
                if(!visitedPoints.has(downPoint) && !pointsToSearch.includes(downPoint)) {
                    pointsToSearch.push(downPoint);
                }
            }
        }
        return includedPoints.size;
    }
    testB() {
        let lowPoints = [];
        for(let i=0; i<this.map.length; ++i) {
            let row = this.map[i];
            this.pointMap[i] = [];
            for(let j=0; j<row.length; ++j) {
                let height = row[j];
                let isLowPoint = true;
                this.pointMap[i][j] = { x:j, y:i, height: height }
                // Left
                if(j>0) {
                    isLowPoint &= row[j-1] > row[j];
                }
                // Right
                if(j<row.length-1) {
                    isLowPoint &= row[j+1] > row[j];
                }
                // Up
                if(i>0) {
                    isLowPoint &= this.map[i-1][j] > row[j];
                }
                // Down
                if(i<this.map.length-1) {
                    isLowPoint &= this.map[i+1][j] > row[j];
                }
                if(isLowPoint){
                    lowPoints.push(this.pointMap[i][j]);
                }
            }
        }
        // console.log(this.map, this.pointMap);
        let basinSizes = [];
        for(let point of lowPoints) {
            let basinSize = this.calculateBasin(point);
            // console.log("Basin size:", point, basinSize);
            basinSizes.push(basinSize);
        }
        basinSizes.sort((a,b) => b-a);
        console.log(basinSizes);
        let danger = basinSizes[0] * basinSizes[1] * basinSizes[2];
        return danger;
    }
}

export default new Day9TestClass();