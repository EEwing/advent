import fs from 'fs';
import { urlToHttpOptions } from 'url';
import TestClass from "../../testclass.js"

function printMap(map, padding=4) {
    console.log("-".repeat(padding*map[0].length + 2));
    for(let row of map) {
        let rowStr = "|";
        for(let col of row) {
            rowStr += col.toString().padEnd(padding);
        }
        console.log(rowStr + "|");
    }
    console.log("-".repeat(padding*map[0].length + 2));
}

function dumpMap(map, delimiter, fileName) {
    let mapStr = map.map(row => row.join(delimiter)).join("\n");
    fs.writeFileSync(fileName, mapStr);
}

class Packet {
    constructor(data) {
        this.version = undefined;
        this.typeId = undefined;
        this.cursor = 0;
        this.subPackets = [];
        this.headerData = data.slice(0, 6);
        this.bodyData = data.slice(6);
        this.parseHeader();
        this.parseBody();
    }
    bitsRemaining() {
        return this.bodyData.length - this.cursor;
    }
    seek(position) {
        this.cursor = position;
    }
    peek(numBits) {
        numBits = Math.min(numBits, this.bitsRemaining());
        if(numBits == 0) {
            console.log("Not reading anything!");
            return "";
        }
        return this.bodyData.slice(this.cursor, this.cursor+numBits);
    }
    readBits(numBits) {
        let bitData = this.peek(numBits);
        this.cursor += numBits;
        return bitData;
    }
    parseHeader() {
        this.version = parseInt(this.headerData.slice(0, 3), 2);
        this.typeId = parseInt(this.headerData.slice(3, 6), 2);
    }
    sumVersions() {
        let sum=this.version;
        for (let packet of this.subPackets) {
            sum += packet.sumVersions();
        }
        return sum;
    }
    parseBody(data) {
        switch(this.typeId) {
            case 4:
                // Literal
                let group = undefined;
                let number = "";
                do {
                    group = this.readBits(5);
                    number += group.slice(1, 5);
                } while (group[0] == '1');
                this.value = parseInt(number, 2);
                break;
            default:
                // Operator
                this.lengthTypeId = parseInt(this.readBits(1));
                this.subPackets = [];
                if(this.lengthTypeId == 0) {
                    let bitLength = parseInt(this.readBits(15), 2);
                    console.log(this.version, "Bit length:", bitLength);
                    console.log(this.peek(bitLength));

                    let bitsRemaining = bitLength;
                    while(bitsRemaining > 6 && parseInt(this.peek(bitsRemaining), 2) != 0) {
                        let packet = new Packet(this.peek(bitLength));
                        console.log(packet);
                        this.subPackets.push(packet);
                        let bitsRead = packet.headerData.length + packet.cursor;
                        this.seek(this.cursor + bitsRead);
                        bitsRemaining -= bitsRead;
                    }
                } else {
                    let numSubPackets = parseInt(this.readBits(11), 2);
                    for(let i=0; i<numSubPackets; ++i) {
                        let subPacket = new Packet(this.peek(this.bitsRemaining()));
                        console.log(subPacket);
                        this.subPackets[i] = subPacket;
                        let bitsRead = subPacket.headerData.length + subPacket.cursor;
                        this.seek(this.cursor + bitsRead);
                    }
                }
                break;
        }
    }
    getValue() {
        switch(this.typeId) {
            case 0:
                // Sum
                let sum = this.subPackets.reduce(((prev, current) => prev + current.getValue()), 0);
                return sum;
            case 1:
                // Product
                let product = this.subPackets.reduce(((prev, current) => prev * current.getValue()), 1);
                return product;
            case 2:
                // minimum
                let min = this.subPackets.reduce(((prev, current) => Math.min(prev, current.getValue())), Number.MAX_VALUE);
                return min;
            case 3:
                // maximum
                let max = this.subPackets.reduce(((prev, current) => Math.max(prev, current.getValue())), Number.MIN_VALUE);
                return max;
            case 4:
                return this.value;
            case 5:
                // greater than
                return this.subPackets[0].getValue() > this.subPackets[1].getValue() ? 1 : 0;
            case 6:
                // less than
                return this.subPackets[0].getValue() < this.subPackets[1].getValue() ? 1 : 0;
            case 7:
                // equal to
                return this.subPackets[0].getValue() == this.subPackets[1].getValue() ? 1 : 0;
        }
    }
}

class Day14TestClass extends TestClass {
    constructor() {
        super();
    }
    hexToBin(hex) {
        let groupSize = 4;
        let numGroups = Math.ceil(hex.length / groupSize);
        let binRtn = "";
        for(let i=0; i<numGroups; ++i) {
            let digits = hex.slice(i*groupSize, Math.min((i+1)*groupSize, hex.length));
            let binDigits = parseInt(digits, 16).toString(2).padStart(4*digits.length, '0');
            console.log(digits,"=>",binDigits);
            binRtn += binDigits;
        }
        binRtn = binRtn.padStart(Math.ceil(binRtn.length/4)*4, '0'); // Realign the bits
        return binRtn;
    }
    init() {
        const rawInput = fs.readFileSync('input.txt', 'utf-8');
        this.inputHex = rawInput.split('\r\n')[0];
    }
    testA() {
        let inputBin = this.hexToBin(this.inputHex);
        console.log(inputBin);
        let packet = new Packet(inputBin);
        console.log(packet);
        return packet.sumVersions();
    }
    testB() {
        let inputBin = this.hexToBin(this.inputHex);
        console.log(inputBin);
        let packet = new Packet(inputBin);
        console.log(packet);
        return packet.getValue();
    }
}

export default new Day14TestClass();